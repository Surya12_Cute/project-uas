package com.fiture.projectuas;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;



import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    private TextView Notifier;
    private ListView ContentList;
    private RequestQueue mQueue;

//    Map<String, String> ArrayContent = new HashMap<String, String>();

    ArrayList arrayList = new ArrayList<>();





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("Berita Hari Ini");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Notifier = (TextView) findViewById(R.id.Notifier);
        ContentList = (ListView) findViewById(R.id.ContentList);


        mQueue = Volley.newRequestQueue(this);

        JsonParse();

        ContentList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);

                ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>(arrayList) ;


                String  SelectedData = (String) ContentList.getItemAtPosition(position);

                for(HashMap<String, String> dataNews :  data){

                    if(SelectedData == dataNews.get("title")){
                        intent.putExtra("DescriptionBerita", String.valueOf(dataNews.get("description")));
                        intent.putExtra("SourceBerita", String.valueOf(dataNews.get("source")));
                        intent.putExtra("SumberGambar", String.valueOf( dataNews.get("SumberGambar")));

                    }

                }

                startActivity(intent);
            }
        });


    }

    private void JsonParse() {

        String url = "https://newsapi.org/v2/top-headlines?country=ID&apiKey=fa37761d82394d688eb92ea5b16f18f8";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ArrayList dataForListView = new ArrayList<>();
                        try {
                            JSONArray jsonArray = response.getJSONArray("articles");
                            for(int i = 0 ; i < jsonArray.length(); i++){
                                JSONObject DataNews  = jsonArray.getJSONObject(i);

                                HashMap<String, String> data = new HashMap<>();

                                data.put("title", DataNews.getString("title"));
                                data.put("description", DataNews.getString("description"));
                                data.put("source", DataNews.getJSONObject("source").getString("name"));
                                data.put("SumberGambar", DataNews.getString("urlToImage"));


                                dataForListView.add(DataNews.getString("title"));
                                arrayList.add(data);

                            }
                            Notifier.setText("");
                        }catch (JSONException e){
                            Notifier.setText("Sedang ada kendala, Coba lagi nanti.");
                        }
                        ArrayAdapter arrayAdapter = new ArrayAdapter(
                                getApplicationContext(),
                                android.R.layout.preference_category,
                                dataForListView
                        );

                        ContentList.setAdapter(arrayAdapter);


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Notifier.setText("Internet Nyalain Anjing !!!");
            }
        });

        mQueue.add(request);

    }



    // aksesoris

    @Override
    protected void onStop(){
        super.onStop();
    }

    //Fires after the OnStop() state
    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            trimCache(this);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }

}





//
//    private void JsonParse() {
//
//        String url = "https://raw.githubusercontent.com/mul14/gudang-data/master/bank/bank.json";
//
//        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null,
//                new Response.Listener<JSONArray>() {
//                    @Override
//                    public void onResponse(JSONArray response) {
//                        try {
//                            for(int i = 0 ; i < response.length(); i++){
//                                JSONObject DataBank  = response.getJSONObject(i);
//                                String BankName = DataBank.getString("name");
//
////                                arrayList.add(BankName);
//                            }
//                            Notifier.setText("");
//                        }catch (JSONException e){
//                            Notifier.setText("Sedang ada kendala, Coba lagi nanti.");
//                        }
//                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_expandable_list_item_1,arrayList);
//                        ContentList.setAdapter(arrayAdapter);
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//                Notifier.setText("Internet Nyalain Anjing !!!");
//            }
//        });
//
//        mQueue.add(request);
//
//    }
